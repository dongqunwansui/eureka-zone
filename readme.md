# eureka 分区测试

### 相关配置（只列出与zone相关的配置）
Eureka1
```yaml
eureka:
  instance:
    hostname: xiaomabenteng
  client:
    service-url:
      zone-1: http://xiaomabenteng:10001/eureka
      zone-2: http://xiaomabenteng:10002/eureka
    region: changsha
    availability-zones:
      changsha: zone-1,zone-2
```


Eureka2
```yaml
eureka:
  instance:
    hostname: xiaomabenteng
  client:
    service-url:
      zone-1: http://xiaomabenteng:10001/eureka
      zone-2: http://xiaomabenteng:10002/eureka
    region: changsha
    availability-zones:
      changsha: zone-2,zone-1
```


Server1
```yaml
eureka:
  instance:
    metadata-map:
      zone: zone-1
    hostname: xiaomabenteng
  client:
    service-url:
      zone-1: http://xiaomabenteng:10001/eureka,http://xiaomabenteng:10002/eureka
      zone-2: http://xiaomabenteng:10002/eureka,http://xiaomabenteng:10001/eureka
    region: changsha
    availability-zones:
      changsha: zone-1,zone-2 
```
      
Server2
```yaml
eureka:
  instance:
    metadata-map:
      zone: zone-2
    hostname: xiaomabenteng
  client:
    service-url:
      zone-1: http://xiaomabenteng:10001/eureka,http://xiaomabenteng:10002/eureka
      zone-2: http://xiaomabenteng:10002/eureka,http://xiaomabenteng:10001/eureka
    region: changsha
    availability-zones:
      changsha: zone-2,zone-1 
```

Customer
```yaml
eureka:
  instance:
    metadata-map:
      zone: zone-2
    hostname: xiaomabenteng
  client:
    service-url:
      zone-1: http://xiaomabenteng:10001/eureka
      zone-2: http://xiaomabenteng:10002/eureka
    region: changsha
    availability-zones:
      changsha: zone-1,zone-2
```

server1分在zone-1区，server2分在zone-2区，customer在zone-2区，服务启动后customer只会调用同在zone-2的server2，只有server2挂了，才会调用server1
需要注意的是zone1区的server1与zone2区的zone2要相互注册，否则customer和server2所在的eureka服务列表中没有server1，自然也就无法调用到server1