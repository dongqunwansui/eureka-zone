package demo.server2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dongqun
 * @date 2020/11/5 17:04
 */
@RestController
public class Endpoint {
    @Value("${zone.name}")
    private String zone;

    @GetMapping("/zone")
    public String getZone(){
        return zone;
    }
}
