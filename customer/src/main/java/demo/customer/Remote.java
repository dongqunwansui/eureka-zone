package demo.customer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author dongqun
 * @date 2020/11/6 9:18
 */
@FeignClient(value = "service")
public interface Remote {
    @GetMapping("/zone")
    String getServiceZone();
}
