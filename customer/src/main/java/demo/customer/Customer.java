package demo.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author dongqun
 * @date 2020/11/5 17:08
 */
@RestController
public class Customer {
    @Autowired
    private Remote remote;

    @GetMapping("hi")
    public String custom(){
        return remote.getServiceZone();
    }
}
